#include <cppshell/shell.hpp>

#include <sys/stat.h>
#include <unistd.h>

#if defined(__linux__)
#include <wait.h>
#elif defined(__APPLE__)
#include <sys/wait.h>
#else
#error Supported only on Linux and Mac
#endif

#include <cstdio>
#include <stdexcept>

namespace cppshell {
Shell::Shell(const std::string& shell, const std::string& path_to_tmp_directory) {
  MakeUniqueDirectory(path_to_tmp_directory);
  MakeFifoFile();

  int fds_exit_codes[2];
  pipe(fds_exit_codes);

  pid_shell_process_ = fork();
  if (pid_shell_process_ == -1) {
    throw std::logic_error("fork failed");
  }

  if (pid_shell_process_ == 0) {  // child
    close(fds_exit_codes[0]);

    execlp("bash", "bash", command_transmission_file_.data(), NULL);
  } else {  // parent
    fd_exit_codes_transmission_ = fds_exit_codes[1];

    close(fds_exit_codes[1]);
    exit_codes_receiving_ = fdopen(fds_exit_codes[0], "r");

    command_transmission_.open(command_transmission_file_);
  }
}

void Shell::MakeUniqueDirectory(const std::string& path_to_tmp_directory) {
  unique_directory_ = path_to_tmp_directory + "/cppshell_XXXXXX";
  if (mkdtemp(const_cast<char*>(unique_directory_.data())) == nullptr) {
    throw std::logic_error("make unique directory failed");
  }
}

void Shell::MakeFifoFile() {
  command_transmission_file_ = unique_directory_ + "/commands";
  if (mkfifo(command_transmission_file_.data(), 0600) == -1) {
    throw std::logic_error("make fifo failed");
  }
}

void Shell::Execute(const std::string& command) {
  command_transmission_ << command << std::endl;
}

int Shell::GetExitCodeLastCommand() {
  Execute("echo $?>&" + std::to_string(fd_exit_codes_transmission_));

  int result;
  int x = fscanf(exit_codes_receiving_, "%d", &result);

  return result;
}

void Shell::Destroy() {
  Execute("exit");

  command_transmission_.close();

  waitpid(pid_shell_process_, 0, 0);

  unlink(command_transmission_file_.data());
  rmdir(unique_directory_.data());
}
}  // namespace cppshell
