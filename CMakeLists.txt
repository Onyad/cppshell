cmake_minimum_required(VERSION 3.14)

project(cppshell)

set(CMAKE_CXX_STANDARD 17)

file(GLOB_RECURSE SOURCES_FILES src/*)

add_library(cppshell ${SOURCES_FILES})
target_include_directories(cppshell PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/include)

