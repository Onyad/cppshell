#pragma once

#include <string>
#include <fstream>

#if !defined(__linux__) && !defined(__APPLE__)
#error Only linux and Macos supported
#endif

namespace cppshell {
class Shell {
 public:
  Shell(const std::string& shell = "bash", const std::string& path_to_tmp_directory = "/tmp");

  void Execute(const std::string& command);

  int GetExitCodeLastCommand();

  ~Shell() {
    Destroy();
  }

 private:
  void Destroy();

  void MakeUniqueDirectory(const std::string& path_to_tmp_directory);
  void MakeFifoFile();

 private:
  std::string unique_directory_;
  std::string command_transmission_file_;

  pid_t pid_shell_process_;
  int fd_exit_codes_transmission_;

  std::ofstream command_transmission_;
  FILE* exit_codes_receiving_;
};
}  // namespace cppshell
